package HW7;

import java.util.Scanner;

public class Main{

    private String phrase;
    private String key;

    public Scanner scanner = new Scanner(System.in);

    public String getPhrase(){
        System.out.println("Enter the encryption phrase");
        String phrase = String.valueOf(this.scanner);
        return phrase;
    }


    public String getKey() {
        System.out.println("Enter the encryption key");
        String key = String.valueOf(this.scanner);
        return key;
    }

    public static void main(String[] args) {

    }

}
