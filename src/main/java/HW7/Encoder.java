package HW7;

import java.util.Scanner;

public class Encoder extends Main {

    Main encoder = new Main();

    public Encoder(Main encoder) {
        this.encoder = encoder;
    }

    public String encoder() {
        char[] charPhrase = encoder.getPhrase().toCharArray();
        char[] encoderStringPhrase = new char[charPhrase.length];
        char[] charKey = encoder.getKey().toCharArray();
        for (int i = 0; i < charPhrase.length; i++) {
            encoderStringPhrase[i] = (char) (charPhrase[i] + charKey[i]);
        }
        return new String(encoderStringPhrase);

    }
}




